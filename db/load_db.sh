#!/bin/bash
set -e

POSTGRES="psql --username postgres"

echo "Creando base"
$POSTGRES <<EOSQL
CREATE DATABASE arai_documentos OWNER postgres;
EOSQL

gunzip < /dbs/modelo.sql.gz | $POSTGRES arai_documentos

